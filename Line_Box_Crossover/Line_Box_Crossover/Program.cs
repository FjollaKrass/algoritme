﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Line_Box_Crossover
{
    class Program
    {
        //inicializojme parentat
        static int[] Parent1 = new int[10] { 1, 3, 6, 6, 3, 5 , 4 ,1 , 2, 5 };
        static int[] Parent2 = new int[10] { 2, 12, 9, 23, 3, 6, 13, 20, 3, 4 };
        static double[] Child1 = new double [Parent1.Count()];
        static double[] Child2 = new double [Parent1.Count()];


        //metoda per gjenerim te random number
        static double RandomNumber(double minNumber, double maxNumber)
        {
            Random random = new Random();
            return Math.Round(random.NextDouble() * (maxNumber - minNumber) + minNumber,2);
        }

        static double RandomNumberBox(Random random,double minNumber, double maxNumber)
        {
            return Math.Round(random.NextDouble() * (maxNumber - minNumber) + minNumber, 2);
        }

        static void Main(string[] args)
        {
            Console.WriteLine();
            string vlera;
            Console.Write("Shkruaje vlerën e Alfës: ");
            vlera = Console.ReadLine();

            double alpha= double.Parse(vlera);
            double u= RandomNumber(0, 1+2*alpha);

            //gjenerimi i femiut te pare me line crossover (u gjenerohet vetem nje here)
            Console.Write("\n Child 1 with line crossover: ");
            for (int i=0;i<Parent1.Count(); i++)
            {
                Child1[i] = (Parent1[i] - alpha) + (u * (Parent2[i] - Parent1[i]));
                Console.Write("{0}  ", Child1[i]);
                
            }

            //gjenerimi i femiut te dyte me box crossover (u gjenerohet per cdo i)
        
            Console.Write("\n Child 2 with box crossover: ");
            Random random = new Random();
            for (int i = 0; i < Parent1.Count(); i++)
            {
                Child2[i] = (Parent1[i] - alpha) + (RandomNumberBox(random, 0,1+2*alpha) * (Parent2[i] - Parent1[i]));
                Console.Write("{0}  ", Child2[i]);

            }

            Console.ReadLine();
        }


    }
}
